package mostrandodatos;

import java.util.Scanner;

public class MostrandoDatos {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Introduce tu nombre:");
		String nombre = scan.next();
		System.out.println("Introduce tu apellido:");
		String apellido = scan.next();
		
		System.out.println("Nombre: "+nombre);
		System.out.println("Apellido: "+apellido);

		
		scan.close();
	}

}
